# Local Development Environment

## Version

PHP 7.4.19
Nginx 1.20.0
MySQL 8.0
Redis 6.0.5

## Repository

### https://gitlab.com/binhvm.kimson/uplon-theme

## Environment construction

1. Make sure that you can use Docker commands and Make commands.
1. `git clone https://gitlab.com/binhvm.kimson/uplon-dev.git`
1. `cd uplon-dev`
1. `make init`

## Corresponding port of each repository

|URL|Each repository|
|---|---|
|http://localhost:8080|uplon|
