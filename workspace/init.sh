#!/bin/sh

cd /var/www/html/uplon-theme || exit
composer install --ignore-platform-reqs
cp .env.example .env
php artisan migrate
php artisan db:seed
php artisan storage:link
